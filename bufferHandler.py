#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys
import logging
from feedLoader import *
from configLoader import *
import time
from entry import *
from datetime import datetime
from datetime import timezone

class BufferHandler():

    con = None
    f = '%Y-%m-%d %H:%M:%S'

    def __init__(self):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.dbpath = self.config.Get('Path', 'DBPath')

        self.con = lite.connect(self.dbpath + '/data.sqlite')
        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/dblog.txt')

    def __Open__(self):
        self.con = lite.connect(self.dbpath + '/data.sqlite')

    def AddEntry(self, entry):
        try:
            self.__Open__()
            cur = self.con.cursor()
            cur.execute('INSERT INTO entry(id, title, link, description, feedid, updated) VALUES(?, ?, ?, ?, ?, ?)', (entry['id'], entry['title'], entry['link'], entry['description'], entry['feedId'], entry['updated']))
            self.con.commit()
        except lite.Error as e:
            # logging.exception('AddEntry')
            # print e
            pass
        finally:
            if self.con:
                self.con.close()

    def GetTodayEntries(self):
        fl = FeedLoader()
        feeds = fl.GetAllFeeds()
        f = '%Y-%m-%d %H:%M:%S'

        startDate = time.strftime('%Y-%m-%d') + ' 00:00:00'
        endDate = time.strftime('%Y-%m-%d') + ' 23:59:59'

        con = lite.connect(self.dbpath + '/data.sqlite')
        try:
            # con.row_factory = lite.Row
            cur = con.cursor()
            for feed in feeds:
                feed.entries = []
                sql = "SELECT * FROM entry WHERE feedId = '{0}' AND (updated BETWEEN '{1}' AND '{2}') ORDER BY updated DESC".format(feed.id, startDate, endDate)

                cur.execute(sql)
                data = cur.fetchall()

                for row in data:
                    updated = row[3]
                    if ':' not in updated:
                        updated += ' 00:00:00'
                    updated = updated.partition('.')[0]
                    updated = updated.partition('+')[0]
                    updated = datetime.strptime(updated, f)

                    e = Entry()
                    e.id = row[0]
                    e.title = row[1]
                    e.link = row[2]
                    e.updated = datetime(updated.year, updated.month, updated.day, updated.hour, updated.minute, updated.second, 0, timezone.utc)
                    e.description = row[4]
                    feed.entries.append(e)

            return feeds
        except lite.Error as e:
            logging.exception('GetTodayEntries')
        finally:
            if con:
                con.close()

    def TrimBuffer(self):
        fl = FeedLoader()
        feeds = fl.GetAllFeeds()

        for feed in feeds:
            id = feed.id;
            cleanbuffer = feed.cleanBuffer

            if cleanbuffer == 1:
                try:
                    self.__Open__()
                    cur = self.con.cursor()
                    cur.execute("DELETE FROM [entry] WHERE updated < date('now', '-3 day') AND feedid = ?", [id])
                    self.con.commit()
                except:
                    logging.exception('TrimBuffer')
                finally:
                    if self.con:
                        self.con.close()
