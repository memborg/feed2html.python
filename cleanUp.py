#!/opt/local/bin/python
# -*- coding: utf-8 -*-

from configLoader import *
from os.path import isfile, join
import time
from shutil import rmtree

class CleanUp:

    def __init__(self):
        folderName = time.strftime('%Y%m%d')
        config = ConfigLoader()
        dumpPath = config.Get('Path', 'DumpPath')
        path = os.path.join(dumpPath, folderName)

        rmtree(path)
