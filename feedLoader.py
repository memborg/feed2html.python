#!/opt/local/bin/python
# -*- coding: utf-8 -*-
import sqlite3 as lite
import sys
import logging
from configLoader import *
from feed import *
from datetime import datetime
from datetime import timedelta
from datetime import timezone

class FeedLoader:
    con = None
    config = None

    def __init__(self):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.dbpath = self.config.Get('Path', 'DBPath')

        self.con = lite.connect(self.dbpath + '/data.sqlite')
        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/log.txt')

    def __Open__(self):
        self.con = lite.connect(self.dbpath + '/data.sqlite')

    def GetAllFeeds(self):
        sql = 'SELECT * FROM feed ORDER BY lower(title)'
        f = '%Y-%m-%d %H:%M:%S'

        try:
            self.__Open__()
            self.con.row_factory = lite.Row
            cur = self.con.cursor()
            cur.execute(sql)

            data = cur.fetchall()

            feeds = []
            for row in data:
                updated = row['updated']
                if ':' not in updated:
                    updated += ' 00:00:00'
                updated = updated.partition('.')[0]
                updated = updated.partition('+')[0]
                updated = datetime.strptime(updated, f)

                feed = Feed()
                feed.id = row['id']
                feed.title = row['title']
                feed.updated = datetime(updated.year, updated.month, updated.day, updated.hour, updated.minute, updated.second, 0, timezone.utc)
                feed.link = row['link']
                feed.cleanBuffer = row['cleanbuffer']
                feed.tags = row['tags']
                feed.feedType = row['feedtype']
                feeds.append(feed)


            return feeds
        except lite.Error as e:
            print("Error %s: " % e.args[0])
        finally:
            if self.con:
                self.con.close()

    def SetLastUpdated(self, id, updated):
        sql = "UPDATE [feed] SET updated = ? WHERE id = ?"
        try:
            self.__Open__()
            cur = self.con.cursor()
            cur.execute(sql, [updated, id])
            self.con.commit()

        except:
            logging.exception('SetLastUpdated')
        finally:
            if self.con:
                self.con.close()


