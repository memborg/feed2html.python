(function() {
    var convertTitleAttrToFigCaption = function() {
        document.querySelectorAll('img[title]').forEach(function(node, b, c) {
            const title = node.title;
            if (title) {
                const f = document.createElement('figure')
                const fc = document.createElement('figcaption');
                fc.innerHTML = node.title;
                node.removeAttribute('title')

                node.parentNode.appendChild(f);
                f.appendChild(node);
                f.appendChild(fc);
            }
        });
    }

    convertTitleAttrToFigCaption();
})();
