#!/opt/local/bin/python
# -*- coding: utf-8 -*-
from feedLoader import *
from feedChecker import *
from bufferHandler import *
from dumpHtml import *
from upload import *
from cleanUp import *

if __name__ == '__main__':
    fl = FeedLoader()
    feeds = fl.GetAllFeeds()

    for feed in feeds:
        Checker(feed)

    bh = BufferHandler()
    bh.TrimBuffer()

    entries = bh.GetTodayEntries()
    DumpHtml(entries)
    Upload()
    CleanUp()

