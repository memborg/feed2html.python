#!/opt/local/bin/python
# -*- coding: utf-8 -*-
import os
import time
from os import listdir
from os.path import isfile, join
from configLoader import *
import ftplib

class Upload:

    folderName = ''
    path = ''
    config = None

    def __init__(self):
        self.folderName = time.strftime('%Y%m%d')
        self.config = ConfigLoader()
        self.dumpPath = self.config.Get('Path', 'DumpPath')
        self.path = os.path.join(self.dumpPath, self.folderName)

        files = self.__getFiles__();
        self.__ftp__(files)

    def __getFiles__(self):
        return [f for f in listdir(self.path) if isfile(join(self.path, f))]

    def __ftp__(self, files):
        server = self.config.Get('FTP', 'Host')
        username = self.config.Get('FTP', 'User')
        password = self.config.Get('FTP', 'Password')
        rootPath = self.config.Get('FTP', 'Root')
        ftp_connection = ftplib.FTP_TLS(server, username, password)

        uploadPath = rootPath + '/' + self.folderName

        ftp_connection.cwd(rootPath);

        if self.folderName in ftp_connection.nlst():
            ftp_connection.cwd(uploadPath)
        else:
            ftp_connection.mkd(uploadPath)
            ftp_connection.cwd(uploadPath)

        for f in files:
            fh = open(os.path.join(self.path, f), 'rb')
            ftp_connection.storbinary('STOR ' + f, fh)
            fh.close()

        ftp_connection.cwd(rootPath);
        file = open(os.path.join(self.dumpPath, 'index.html'), 'rb')
        ftp_connection.storbinary('STOR index.html', file)
        file.close()

        ftp_connection.quit()
        ftp_connection.close()
