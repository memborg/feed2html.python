#!/opt/local/bin/python
# -*- coding: utf-8 -*-
import time
import os
from configLoader import *
from feedLoader import *
from io import StringIO
import codecs

class DumpHtml:
    feeds = []
    f = '%Y-%m-%d %H:%M:%S'
    c = '%Y%m%d%H%M%S'

    def __init__(self, feeds):
        self.feeds = feeds
        self.__process__()

    def __process__(self):
        fl = FeedLoader()
        config = ConfigLoader()
        logPath = config.Get('Path', 'LogPath')
        dumpPath = config.Get('Path', 'DumpPath')
        mainTemplate = codecs.open(os.path.join(logPath, 'index.html'), 'r', 'utf-8-sig').read()
        template = codecs.open(os.path.join(logPath, 'hourly.html'), 'r', 'utf-8-sig').read()
        rootIndex = codecs.open(os.path.join(logPath, 'rootindex.html'), 'r', 'utf-8-sig').read()

        mainIndex = []
        mainHtml = ''
        folderName = time.strftime('%Y%m%d')
        path = os.path.join(dumpPath, folderName)
        indexPath = os.path.join(path, 'index.html')
        rootIndexPath = os.path.join(dumpPath, 'index.html')

        if not os.path.exists(path):
            os.makedirs(path)

        for feed in self.feeds:
            index = []
            body = []
            count = len(feed.entries)

            for entry in feed.entries:
                index.append('<li><a href="#{0}">{1}</a></li>'.format(entry.id, entry.title))

                body.append("<article>")
                body.append('<a name="{0}"></a>'.format(entry.id))
                body.append("<h2>{0}</h2>".format(entry.title))
                body.append('<span class="updated">{0}</span>'.format(entry.updated.strftime(self.f)))
                body.append("<p>{0}</p>".format(entry.description))
                body.append('<a href="{0}" target="_blank" rel="noopener noreferrer"><strong>Read more</strong></a>'.format(entry.link))
                body.append("</aticle>")
                body.append("<hr/>")

            if count > 0:
                html = template.replace("{tags}", feed.tags)
                html = html.replace("{title}", feed.title)
                html = html.replace("{index}", ''.join(index))
                html = html.replace("{entries}", ''.join(body))

                filename = feed.id + '.html'

                feedPath = os.path.join(path, filename)

                saveFile = codecs.open(feedPath, "w", "utf-8-sig")
                saveFile.write(str(html))
                saveFile.close()

                mainIndex.append('<li><a href="{0}.html?rnd={2}">{1} ({3})</a></li>'.format(feed.id, feed.title, feed.updated.strftime(self.c), count))


        mainHtml = mainTemplate.replace("{title}", folderName)
        mainHtml = mainHtml.replace("{index}", ''.join(mainIndex))
        mainHtml = mainHtml.replace("{updated}", time.strftime(self.f))

        saveFile = codecs.open(indexPath, "w", "utf-8-sig")
        saveFile.write(str(mainHtml))
        saveFile.close()

        rootHtml = rootIndex.replace("{lastestnews}", 'http://news.superrune.dk/' + folderName)

        saveFile = codecs.open(rootIndexPath, "w", "utf-8-sig")
        saveFile.write(str(rootHtml))
        saveFile.close()
