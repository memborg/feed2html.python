#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
from bufferHandler import *
from feedLoader import *
from configLoader import *
from datetime import datetime
from datetime import timedelta
from datetime import timezone
import time
from io import StringIO
import logging
import uuid
import unicodedata, re
import codecs
from urllib.parse import urlparse

class Checker():

    entries = []
    unwantedAttribs = ['width', 'height', 'class', 'lang', 'style', 'size', 'face', 'id', '[owexp]', 'onclick', 'onchange', 'onblur', 'onfocus', 'onsubmit']
    unwantedTags = ['script', 'style']
    userAgent = 'Mozilla/5.0 (Windows NT 10.0; rv:44.0) Gecko/20100101 Firefox/48.0'

    def __init__(self, feed):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.feedId = feed.id
        feedType = feed.feedType
        self.feedTitle = feed.title
        self.feedUpdated = feed.updated
        self.feedLink = feed.link
        self.feedURI = urlparse(self.feedLink)
        self.feedDomain = '{uri.scheme}://{uri.netloc}'.format(uri=self.feedURI)

        datetime.now(timezone.utc)

        fl = FeedLoader();

        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/log.txt')

        ET.register_namespace('', 'http://www.w3.org/2005/Atom');
        ET.register_namespace('xhtml', 'http://www.w3.org/1999/xhtml');

        try:
            self.entries = []

            xml = self.__ReadFeed__(feed.link)

            if xml is not None:
                if feedType == 1:
                    self.__ParseRSS2__(xml)

                if feedType == 2:
                    self.__ParseAtom1__(xml)

                if feedType == 3:
                    self.__ParseRDF__(xml)

                bh = BufferHandler()
                for row in self.entries:
                    bh.AddEntry(row)

                if len(self.entries) > 0:
                    fl.SetLastUpdated(self.entries[0]['feedId'], self.entries[0]['updated'])
        except:
            logging.exception(self.feedTitle + '(' + self.feedLink + ')')

    def __ReadFeed__(self, link):
        req = urllib.request.Request(
            link,
            data = None,
            headers={
                'User-Agent': self.userAgent
            }
        )

        f = urllib.request.urlopen(req)
        charset = f.headers.get_content_charset()
        if charset == None:
            charset = 'utf-8'

        try:
            xml = f.read().decode(charset)
        except:
            xml = f.read().decode('iso8859-1')

        try:
            xml = self.__CleanUpXml(xml)

            if len(xml) == 0:
                return None

            return ET.fromstring(xml)
        except Exception as e:
            saveFile = open(self.logpath + '/' + self.feedTitle + '.xml','w')
            saveFile.write(str(xml))
            saveFile.close()

            logging.exception(self.feedTitle + '('+self.feedLink+')')
            return None

    def __CleanUpHTML__(self, html):
        html = re.sub(r'(class|id|style|height|width|sizes|srcset|lang|face|onclick|onchange|onblur|onsubmit|onfocus|\[ovwxp\])="[^"]+"', '', html)
        html = re.sub(r'\.\./\.\.', self.feedDomain, html)
        html = re.sub(r'src="/ckfinder', 'src="' + self.feedDomain + '/ckfinder', html)
        html = re.sub(r'src="/wp-content', 'src="' + self.feedDomain + '/wp-content', html)
        html = re.sub(r'src="/images', 'src="' + self.feedDomain + '/images', html)
        html = re.sub(r"\\n",'', html)
        html = re.sub(r"\\r",'', html)
        html = re.sub(r"\\t",'', html)
        html = re.sub(r"\\'", '', html)
        html = html.strip()

        return html

    def __CleanUpXml(self, xml):
        charsToRemove = ['16'];
        # for c in charsToRemove:
        #     xml = xml.replace(c.decode('hex'), '')

        # xml = xml.replace('\r',' ')
        # xml = xml.replace('\n', ' ');
        #
        return xml.strip()
    def __ParseRDF__(self, xml):
        ns = {
            'dc': 'http://purl.org/dc/elements/1.1/',
            'slash': 'http://purl.org/rss/1.0/modules/slash/',
            'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            'ns1': 'http://purl.org/rss/1.0/'
        }

        nodes = xml.findall('ns1:item', ns)
        date = datetime.now()
        date = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, 0, timezone.utc)

        for node in nodes:
            if node.find('dc:date', ns) is not None:
                date = self.ParseDate(node.find('dc:date', ns).text)

            if date >= self.feedUpdated:
                n = {
                    'title': ''.join(node.find('ns1:title', ns).itertext()),
                    'link': node.find('ns1:link', ns).text,
                    'id': str(uuid.uuid4()),
                    'feedId': self.feedId,
                    'description': ''
                }

                n['updated'] = date

                description = node.find('ns1:description', ns)

                if description is not None:
                    descriptionText = self.__CleanUpHTML__(''.join(description.itertext()))
                    n['description'] += descriptionText

                self.entries.append(n)

    def __ParseRSS2__(self, xml):
        ns = {
            'DR': 'http://dr.dk/channel/2.0/modules/dr/',
            'content': 'http://purl.org/rss/1.0/modules/content/',
            'dc': 'http://purl.org/dc/elements/1.1/',
            'atom': 'http://www.w3.org/2005/Atom',
            'sy': 'http://purl.org/rss/1.0/modules/syndication/',
            'slash': 'http://purl.org/rss/1.0/modules/slash/',
            'media': 'http://search.yahoo.com/mrss/',
            'a10': 'http://www.w3.org/2005/Atom',
            'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            'ns1': 'http://purl.org/rss/1.0/'
        }
        nodes = xml.findall('channel/item', ns)

        date = datetime.now()
        date = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, 0, timezone.utc)

        for node in nodes:
            if node.find('dc:date', ns) is not None:
                date = self.ParseDate(node.find('dc:date', ns).text)

            if node.find('a10:updated', ns) is not None:
                date = self.ParseDate(node.find('a10:updated', ns).text)

            if node.find('updated', ns) is not None:
                date = self.ParseDate(node.find('updated', ns).text)

            if node.find('pubDate', ns) is not None:
                date = self.ParseDate(node.find('pubDate', ns).text)

            if date >= self.feedUpdated:
                n = {
                        'title': ''.join(node.find('title', ns).itertext()),
                        'link': node.find('link', ns).text,
                        'id': str(uuid.uuid4()),
                        'feedId': self.feedId,
                        'description': ''
                    }

                n['updated'] = date

                content = node.find('content:encoded', ns)
                description = node.find('description', ns)

                contentText = ''
                descriptionText = ''

                if content is not None:
                    contentText = self.__CleanUpHTML__(''.join(content.itertext()))

                if description is not None:
                    descriptionText = self.__CleanUpHTML__(''.join(description.itertext()))

                if len(descriptionText) > len(contentText):
                    n['description'] += descriptionText
                else:
                    n['description'] += contentText

                if node.find('image', ns) is not None:
                    n['description'] += '<p><img src="'+ node.find('image', ns).text +'"/></p>'

                enclosures = node.findall('enclosure', ns)
                for enclosure in enclosures:
                    mimetype = enclosure.attrib['type']
                    src = enclosure.attrib['url']
                    if mimetype == 'image/jpeg':
                        n['description'] += '<p><img src="'+src+'"/></p>'
                    elif mimetype == 'image/jpg':
                        n['description'] += '<p><img src="'+src+'"/></p>'
                    elif mimetype == 'image/png':
                        n['description'] += '<p><img src="'+src+'"/></p>'
                    elif mimetype == 'image/gif':
                        n['description'] += GetGIFHTML(src)
                    elif mimetype == 'video/mp4':
                        n['description'] += '<p><video controls="controls"><source src="'+src+'" type="'+mimetype+'"/></video></p>'
                    elif mimetype == 'video/mov':
                        n['description'] += '<p><video controls="controls"><source src="'+src+'" type="video/mp4"/></video></p>'
                    elif mimetype == 'audio/mpeg':
                        n['description'] += '<p><audio controls="controls"><source src="' + src + '" type="audio/mp3"/></audio></p>'
                    else:
                        n['description'] += '<p><b>Enclosure not handled <a href="'+src+'">'+src+'</a> '+mimetype+'</b></p>'

                imageUris = node.findall('DR:XmlImageArticle/DR:ImageUri620x349', ns)
                for imageUri in imageUris:
                    if imageUri.text is not None:
                        n['description'] += '<p><img src="' + imageUri.text + '"/></p>'

                mediaContents = node.findall('media:content', ns)
                for mediaContent in mediaContents:
                    if 'url' in mediaContent.attrib:
                        contentUrl = mediaContent.attrib['url']
                        contentMedium = 'no medium found'
                        if 'medium' in mediaContent.attrib:
                            contentMedium = mediaContent.attrib['medium']
                        if 'type' in mediaContent.attrib:
                            contentMedium = mediaContent.attrib['type']

                        if 'gravatar' not in contentUrl:
                            if contentMedium == 'image':
                                n['description'] += '<p><img src="' + contentUrl + '"/></p>'
                            elif contentMedium == 'image/jpg':
                                n['description'] += '<p><img src="' + contentUrl + '"/></p>'
                            elif contentMedium == 'image/jpeg':
                                n['description'] += '<p><img src="' + contentUrl + '"/></p>'
                            elif contentMedium == 'image/png':
                                n['description'] += '<p><img src="' + contentUrl + '"/></p>'
                            elif contentMedium == 'image/gif':
                                n['description'] += GetGIFHTML(contentUrl)
                            else:
                                n['description'] += '<p><b>Media Content not handled <a href="'+ contentUrl +'">' + contentUrl + '</a> ' + contentMedium + '</b></p>'

                self.entries.append(n)

    def __ParseAtom1__(self, xml):
        ns = {
            'yt': 'http://www.youtube.com/xml/schemas/2015',
            'content': 'http://purl.org/rss/1.0/modules/content/',
            'media': 'http://search.yahoo.com/mrss/',
        }
        xmlns = '{http://www.w3.org/2005/Atom}'
        nodes = xml.findall(xmlns + 'entry', ns)

        className = re.sub(r'\s', '-', self.feedTitle.lower())

        date = datetime.now()
        date = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, 0, timezone.utc)

        for node in nodes:
            dateElm = node.find(xmlns + 'published')
            updatedElm = node.find(xmlns + 'updated')

            if updatedElm is not None:
                if updatedElm.text is not None:
                    date = self.ParseDate(updatedElm.text)

            if dateElm is not None:
                if dateElm.text is not None:
                    date = self.ParseDate(dateElm.text)

            if date >= self.feedUpdated:
                n = {
                        'title': node.find(xmlns + 'title', ns).text,
                        'link': node.find(xmlns + 'link', ns).attrib['href'],
                        'id': str(uuid.uuid4()),
                        'feedId': self.feedId,
                        'description': ''
                    }

                n['updated'] = date
                content = node.find(xmlns + 'content')

                if content is not None:
                    contentType = content.attrib['type']

                    if contentType is not None:
                        if contentType == 'xhtml':
                            text = str(ET.tostring(content, 'latin1', 'html'))
                            text = re.sub(r'xhtml:','', text)
                            text = re.sub("b'",'', text)
                            n['description'] += self.__CleanUpHTML__(text)
                        elif contentType == 'html':
                            n['description'] += self.__CleanUpHTML__(''.join(content.itertext()))
                        elif contentType == 'text':
                            n['description'] += self.__CleanUpHTML__(''.join(content.itertext()))
                        else:
                            n['description'] += '<p><b>CONTENT TYPE "' + contentType + '" NOT SUPPORTED YET</b></p>'
                            n['description'] += self.__CleanUpHTML__(''.join(content.itertext()))


                    else:
                        n['description'] += self.__CleanUpHTML__(''.join(content.itertext()))

                elif node.find(xmlns + 'summary', ns) is not None:
                    n['description'] += self.__CleanUpHTML__(''.join(node.find(xmlns + 'summary', ns).itertext()))

                group = node.find('media:group', ns)
                if group is not None:
                    thumb = group.find('media:thumbnail', ns)
                    description = group.find('media:description', ns)
                    thumbUrl = thumb.attrib['url']

                    n['description'] += '<p><img src="' + thumbUrl + '"/></p>'

                    if description.text is not None:
                        n['description'] += '<p class="' + className + '">' + description.text + '</p>'

                self.entries.append(n)

    def GetGIFHTML(gifUrl):
        html = '<p><video preload="auto" autoplay muted loop webkit-playsinline playsinline>'
        html += '<source src="' + gifUrl.replace('.gif', '.mp4') + '" type="video/mp4" />'
        html += '<source src="' + gifUrl.replace('.gif', '.webm') + '" type="video/webm" />'
        html += '<img src="' + gifUrl + '"/>'
        html += '</video></p>'

        return html

    def ParseDate(self, strDate):
        #Date parsing in python is not that elegant and my handling is pretty crude to say the least.
        #And I didn't use any modules?!

        hours = self.ParseTimeZone(strDate)

        date = strDate
        date = date.replace('GMT','')
        date = date.replace('PST','')
        date = date.replace('PDT','')
        date = date.replace('EST','')
        date = date.replace('EDT','')
        date = date.replace('Z', '')
        date = self.ConvertDayName(date) #Convert DANISH day name to english
        date = self.ConvertMonthName(date) #Convert DANISH month names into english
        date = date.strip()

        format = ''
        newdate = None
        success = 0

        if success == 0:
            try:
                format = '%d %b %Y'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%Y-%m-%dT%H:%M:%S'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%Y-%m-%dT%H:%M:%S.%f'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%a, %d %b %Y %H:%M:%S'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%b %d, %Y'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%a, %d %b %Y %H:%M:%S %Z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%a, %d %b %Y %H:%M:%S %z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        #UTC offset in the form +HHMM or -HHMM (empty string if the the object is naive).
        if success == 0:
            try:
                date = self.HandleOffset(date)
                format = '%Y-%m-%dT%H:%M:%S%z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        #UTC offset in the form +HHMM or -HHMM and microseconds (empty string if the the object is naive).
        if success == 0:
            try:
                date = self.HandleOffset(date)
                format = '%Y-%m-%dT%H:%M:%S.%f%z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        #Time zone name and microseconds (empty string if the object is naive).
        if success == 0:
            try:
                date = self.HandleOffset(date)
                format = '%Y-%m-%dT%H:%M:%S.%f%Z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        #Time zone name (empty string if the object is naive).
        if success == 0:
            try:
                date = self.HandleOffset(date)
                format = '%Y-%m-%dT%H:%M:%S%Z'
                newdate = datetime.strptime(date, format)
                success = 1
            except:
                success = 0

        if success == 0:
            newdate = datetime.now()
            raise ValueError("Couldn't parse date", strDate, date)
        else:
            newdate = newdate + timedelta(hours=hours)
            newdate = newdate + timedelta(hours=time.daylight)

        return datetime(newdate.year, newdate.month, newdate.day, newdate.hour, newdate.minute, newdate.second, 0, timezone.utc)

    def ConvertDayName(self, name):
        name = name.replace('man', 'Mon')
        name = name.replace('tir', 'Tue')
        name = name.replace('ons', 'Wed')
        name = name.replace('tor', 'Thu')
        name = name.replace('fre', 'Fri')
        name = name.replace('lør', 'Sat')
        name = name.replace('søn', 'Sun')

        return name

    def ConvertMonthName(self, month):
        month = month.replace('maj', 'May')
        month = month.replace('okt', 'Oct')

        return month

    def HandleOffset(self, strDate):
        if 'T' in strDate:
            splits = strDate.split('T');
            t = splits[1]

            o = ''
            s = ''
            t2 = t
            if '+' in t:
                splits2 = t.split('+')
                o = splits2[1].replace(':','')
                t2 = splits2[0]
                s = '+'
            if '-' in t:
                splits2 = t.split('-')
                o = splits2[1].replace(':','')
                t2 = splits2[0]
                s = '-'

            strDate = splits[0] + 'T' + t2 + s + o

        return strDate

    def ParseTimeZone(self, date):
        hours = 0;
        if 'PST' in date:
            hours = 8
        if 'PDT' in date:
            hours = 7
        if 'EDT' in date:
            hours = 4
        if 'EST' in date:
            hours = 5
        if 'GMT' in date:
            hours = 1

        return hours;

